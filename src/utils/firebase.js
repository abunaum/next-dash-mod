import { getAnalytics } from 'firebase/analytics'
import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'

const firebaseConfig = {
  apiKey: 'AIzaSyDZpA-GnbzA-J8N9VXpcwEjJtV-tYtBsc4',
  authDomain: 'modin-v2.firebaseapp.com',
  projectId: 'modin-v2',
  storageBucket: 'modin-v2.appspot.com',
  messagingSenderId: '113052537874',
  appId: '1:113052537874:web:4f636a671701a20bc574cb',
  measurementId: 'G-9FZXW20G1Y'
}

export const app = initializeApp(firebaseConfig)
export const db = () => {
  return getFirestore(app)
}

export const analytics = () => {
  return getAnalytics(app)
}

export const initFirebase = () => {
  return app
}
